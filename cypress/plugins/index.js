// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)

module.exports = (on, config) => {
  // `on` is used to hook into various events Cypress emits
  // `config` is the resolved Cypress config
}

// In cypress/plugins/index.js
let percyHealthCheck = require('@percy/cypress/task')

module.exports = (on, config) => {
  on("task", percyHealthCheck);
};

/*
// in plugins/index.js
const fs = require('fs')

module.exports = (on, config) => {
  on('task', {
    getPercyURLS (filename) {
      if (fs.existsSync(filename)) {
        var data = fs.readFileSync(filename, 'utf8');
        return data
      }
      else{
        return 'Task name: getPercyURLS (plugins/index.js), file provided not found!'
      }
    }
  })
}
*/