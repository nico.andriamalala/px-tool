/// <reference types="Cypress" />

const utils = require('../../global_functions/utils');

//Going to the first Article page
const goToArticlePage = (environment, country) => {
  //Initialize fixture to retrieve url for Article
  cy.fixture('restage/urls').then(($urls) => {
    const base = $urls[environment][country]['base']
    const relativePath = $urls[environment][country]['articleTemplate1']
    cy.visit("http://localhost:3000/");
  })
}

//verify header
const verifyHeader = (mobile) =>{
    const header = cy.get('header[class="l-header oasis"][role="banner"]').then(($header) =>{
        expect($header).to.exist;
        return header;
    })
    const nav = header.within(() => {
        return cy.get('nav[class="nav open"]').then(($nav) =>{
            expect($nav).to.exist;
            return $nav;
        })
    })
    const a = nav.within(() => {
        return cy.get('a').then(($a) => {
            expect($a).to.exist;
            return $a
        })
    })
    const h1 = a.within(() => {
        return cy.get('h1[class="logo__container"]').then(($h1) => {
            expect($h1).to.exist;
            return $h1;
        })
    })
    h1.within(() => {
        cy.get('span[class="sr-only"]').then(($span) => {
            expect($span.text()).not.to.equal('');
        })
    })
}

// Signup
const verifySignup = (mobile) => {
    //54. Signup Component_Verify the email field is present
    cy.xpath('.//*[contains(@class,"signup-btn-container")]/div[@class="form-group form-group-flex"]/span[@class="signup-label"]')
    .should('not.have.text', '')
    cy.xpath('.//*[contains(@class,"signup-btn-container")]/div[@class="form-group form-group-flex"]/input[@class="search form-control"]')
    
    //55. Signup Component_Verify when the email field is blank and CTA button is clicked then an error message is present
    cy.xpath('.//*[contains(@class,"signup-btn-container")]/div[@class="signup-form-btn"]/button')
    .click()

    cy.xpath('.//*[contains(@class,"signup-btn-container")]/div[@class="form-group form-group-flex"]/span[@class="err-msg"]')
    .should('not.have.text', '')


    //56. Signup Component_Verify when the email field has an incorrect email format and CTA button is clicked then another error message is displayed
    var form = cy.get('form[class="signup-btn-container"]').then(($form) => {
        expect($form).to.exist
        return $form
    })
    var textBefore;
    form.within(() => {
        cy.get('span[class="err-msg"]').then(($span) =>{
            expect($span).to.exist
            textBefore = $span.text()
        })
    }).then(() => {
        cy.xpath('.//*[contains(@class,"signup-btn-container")]/div[@class="form-group form-group-flex"]/input[@class="search form-control border-danger"]')
        .type('Hello')
        cy.xpath('.//*[contains(@class,"signup-btn-container")]/div[@class="signup-form-btn"]/button')
        .click()
        cy.xpath('.//*[contains(@class,"signup-btn-container")]/div[@class="form-group form-group-flex"]/span[@class="err-msg"]')
        .should('not.have.text', textBefore)
    })

    //57. Signup Component_Verify when a correct formatted email address is present and CTA button is clicked then no error messages must be present
    cy.xpath('.//*[contains(@class,"signup-btn-container")]/div[@class="form-group form-group-flex"]/input[@class="search form-control border-danger"]')
    .type('test5589@gmail.com')
    cy.xpath('.//*[contains(@class,"signup-btn-container")]/div[@class="signup-form-btn"]/button')
    .click()
    cy.xpath('.//*[contains(@class,"signup-btn-container")]/div[@class="form-group form-group-flex"]/input[@class="search form-control border-success"]')
    cy.xpath('.//*[contains(@class,"signup-btn-container")]/div[@class="form-group form-group-flex"]/span[@class="err-msg"]')
    .should('not.exist')

    

}

// Sticky Summary
const verifyStickySummary = (mobile) => {
    if(!mobile){
        //19. Sticky Summary_Verify the sticky summary is present on desktop
        cy.xpath('.//*[contains(@class,"sticky collapse")]/fieldset/ul/li[3]/button/div')
        
        //20. Sticky Summary_Verify the clicked on a H2 item, the page scrolls to that section on desktop
        cy.xpath('.//*[contains(@class,"sticky collapse")]/fieldset/ul/li[3]/button')
        .click()
        
        //21. Sticky Summary_Verify the H2 item on the sticky summary is active/selected on desktop
        cy.xpath('.//*[contains(@class,"sticky collapse")]/fieldset/ul/li[3]')
        .should('have.class', 'title active')
        
    }
    else
    {
        cy.scrollTo(0, 100);
        //22. Sticky Summary_Verify the sticky summary is present as a nav bar on mobile
        cy.xpath('.//*[contains(@class,"nav open")]/div/div/button/i')
        cy.xpath('.//*[contains(@class,"sticky collapse")]/fieldset/ul/li[3]')
        cy.xpath('.//*[contains(@class,"sticky collapse")]/fieldset/legend')
        .should('not.have.text', '')

        //24. Sticky Summary_Verify when an H2 item is clicked it scrolls done to that section on mobile
        //25. Sticky Summary_Verify the sticky summary closes when the above action occurs
        cy.xpath('.//*[contains(@class,"nav open")]/div[@class="contain"]/div/button')
        .click()
        cy.xpath('.//*[contains(@class,"sticky show")]/div[@class="hide"]/ul/li[3]/button')
        .click()

        //26. Sticky Summary_Verify the H2 item on mobile is active/selected
        cy.xpath('.//*[contains(@class,"nav open")]/div[@class="contain"]/div/button')
        .click()
        cy.xpath('.//*[contains(@class,"sticky show")]/div[@class="hide"]/ul/li[3]')
        .should('have.class', 'title active')

    }
}

// Related Article
const verifyRelatedArticle = (mobile) => {
    var length;
    cy.get('div[class="main"]').within(() => {
        cy.get('div[class="container"]').then(($div) => {
            var i =0;
            length = $div.length
        })
    })
    //40. Related article_Verify the title is present
    cy.get('div[class="main"]').within(() => {
        var i=1
        while(i <= length){
            cy.xpath('(div[@class="container"])['+i+']/div/div/div').within(() =>{
                cy.xpath('h3')
                .should('not.have.text', '')
                var divnumber;
                cy.get('div').within(($div) => {
                    divnumber = $div.length
                    divnumber = divnumber/6
                }).then(() => {
                    var j = 1
                    while(j <= divnumber){
                        // Should be more specific: check src attr if img and check text if h1
                        cy.xpath('div['+j+']/a/div[@class="box-items"]/div[@class="text items"]/div[@class="title"]').within(() => {
                            cy.xpath('img | h1')
                        })

                        //41. Related article_Verify an article is present
                        cy.xpath('div['+j+']/a/div[@class="box-items"]/div[@class="text items"]/h2/strong')
                        .should('not.have.text', '')

                        //42. Related article_Verify the article has a link
                        cy.xpath('div['+j+']/a/div[@class="image items"]').within(() => {
                            cy.get('img').then(($img) =>{
                                expect($img).to.exist
                                expect($img.attr('src')).exist
                                expect($img.attr('src')).not.to.equal('')
                                expect($img.attr('alt')).not.to.equal('')
                            })
                        })
                        
                        j++
                    }
                })
            })
            i++
        }
    })
}
// Verify Author
const verifyAuthor = (mobile) => {
  //27-Author_Verify the section is present

    //28-Author_Verify a hyperlink is present on the Author
    cy.get('.js-article-info a').should('have.attr', 'href');    
}
// Verify BasicBanner
const verifyBasicBanner = (mobile) => {
    // 34 Basic banner_Verify the banner is present
    cy.get('.basic-banner').then(($basicBanner) => {
        expect($basicBanner).to.exist;
    })
    // 35 Basic banner_Verify the banner has a hyperlink
    cy.get('.basic-banner a').should('have.attr', 'href');
    // 36 Basic banner_Verify on click on the banner, it opens the hyperlink on new tab
    cy.get('.basic-banner a').should('have.attr', 'target', '_blank')
}
//Breadcrumb
const verifyBreadcrumbs = (mobile) => {

  //Selectors for Mobile/Desktop
  if (!mobile) { // if the viewport is desktop
    // 14.	Breadcrumb_Verify on desktop the breadcrumb has 4 sections
    // Example ONLY using reference: https://www.pampers.co.uk/pregnancy/baby-shower/article/baby-shower-games
    cy.get('.l-container .c-breadcrumb__list').then(($breadcrumbs) => {
      expect($breadcrumbs).to.contains.text('Home') //Verify Home text is present
      expect($breadcrumbs).to.contains.text('Pregnancy') //Verify Pregnancy text is present
      expect($breadcrumbs).to.contains.text('Baby Shower') //Verify Baby Shower text is present
      expect($breadcrumbs).to.contains.text('11 Types of Fun Baby Shower Games') //Verify Article text is present
    })

    // 15.	Breadcrumb_Verify on desktop the links on breadcrumb is present
    // Some actions here

  } else { // agent is mobile
    // 16.	Breadcrumb_Verify on mobile the breadcrumb consists only category and subcategory
    cy.get('.post__category .category__wrapper').then(($breadcrumbs) => {
      expect($breadcrumbs).does.not.contains.text('Home') //Verify Home text is present
      expect($breadcrumbs).to.contains.text('Pregnancy') //Verify Pregnancy text is NOT present
      expect($breadcrumbs).to.contains.text('Baby Shower') //Verify Baby Shower text is NOT present
      expect($breadcrumbs).does.not.contains.text('11 Types of Fun Baby Shower Games') //Verify Article text is present

    })
    // 17.	Breadcrumb_Verify on mobile the breadcrumb has 2 links
    // Some actions here
  }

}

// CTA
const verifyCTAButton = (mobile) => {

  // 29.	CTA Button_Verify the button is present
  // Some actions here

  // 30.	CTA Button_Verify hyperlink is present
  // Some actions here

  //Selectors for Mobile/Desktop
  if (!mobile) { // if the viewport is desktop

  } else { // agent is mobile

  }

}

const verifyBasicAccordion = () => {
  /// <reference types="Cypress" />

  //FAQ_Verify when clicked on the first FAQ item it closes
  //FAQ_Verify the second FAQ item is still open

  describe('Input form', () => {
    it('focuses input on load', () => {
      cy.visit('https://storybook.pampers.com/iframe.html?id=basic-accordion--faq')
    })

    //FAQ_Verify the component is present
    it('FAQ Present', () => {
      cy.get('.content')
    })


    //FAQ_Verify the first FAQ item is opened by default
    it('FAQ First item open by default', () => {
      cy.xpath('.//*[contains(@class,"content")]/ul/li[1]/button/div')
        .should('have.class', 'arrow-accordion')
    })


    //FAQ_Verify when clicked on the second FAQ item, it opens up
    it('FAQ Open second item', () => {
      cy.xpath('.//*[contains(@class,"content")]/ul/li[2]/button/div')
        .should('have.class', 'arrow-accordion arrow-down')

      cy.xpath('.//*[contains(@class,"content")]/ul/li[2]/button/div')
        .click()
        .should('have.class', 'arrow-accordion')

      //FAQ_Verify the first FAQ item is still open
      cy.xpath('.//*[contains(@class,"content")]/ul/li[1]/button/div')
        .should('have.class', 'arrow-accordion')
    })

    //FAQ_Verify when clicked on the first FAQ item it closes
    //FAQ_Verify the second FAQ item is still open
    it('FAQ close first item', () => {
      cy.xpath('.//*[contains(@class,"content")]/ul/li[1]/button/div')
        .click()
        .should('have.class', 'arrow-accordion arrow-down')

      //FAQ_Verify the first FAQ item is still open
      cy.xpath('.//*[contains(@class,"content")]/ul/li[2]/button/div')
        .should('have.class', 'arrow-accordion')
    })
  })
}

//cypress custom functions
function routeReq(methodd, path, aliasName) {
  // Create a route listener to try and catch
  cy.route({ method: methodd, url: path }).as(aliasName);
}

exports.goToArticlePage = goToArticlePage;
exports.verifyBreadcrumbs = verifyBreadcrumbs;
exports.verifyAuthor = verifyAuthor;
exports.verifyCTAButton = verifyCTAButton;
exports.verifyHeader = verifyHeader;
exports.verifyStickySummary = verifyStickySummary;
exports.verifySignup = verifySignup;
exports.verifyBasicBanner = verifyBasicBanner;
exports.verifyBasicAccordion = verifyBasicAccordion;
exports.verifyRelatedArticle = verifyRelatedArticle;
