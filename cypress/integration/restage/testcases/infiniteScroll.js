/// <reference types="Cypress" />

//Infinite Scroll_Scroll down to the end of the page, verify an article is present.
const verifyArticeIsPresent = (mobile) => {
    it('Infinite Scroll_Scroll down to the end of the page, verify an article is present.',
        () => {
            //cy.visit('http://localhost:9009/iframe.html?id=infinitescroll--basic')
            expect(cy.get('.article.selected')).to.exist;
            cy.get('.article.selected').scrollIntoView({ easing: 'linear', duration: 1000 });
            
        }
    )
}
//Infinite Scroll_Scroll down again and verify another article loads
const scrollToBottotomOfArticle = () => {
    it('Infinite Scroll_Scroll down again and verify another article loads',
        () => {
            cy.get('.article.selected').next().scrollIntoView();
            expect(cy.get('.infinite-item')).to.exist;
        }
    )
}
//Infinite Scroll_Verify a ‘Read more’ article is present.
const scrollToFirstReadMore = () => {
    it('Infinite Scroll_Verify a ‘Read more’ article is present.',
        () => {
            expect(cy.get('.infinite-item button')).to.exist;
            cy.get('.infinite-item:first button').scrollIntoView({ easing: 'linear', duration: 500 , offset:{ top : 100-Cypress.config('viewportHeight') } });
        }

    )
}
//Infinite Scroll_Hover on the first ‘read more’ article and verify the hover effect is present (check class).
const hoverFirstReadMore = () => {
    it('Infinite Scroll_Hover on the first ‘read more’ article and verify the hover effect is present (check class).',
        () => {
            //cy.get('.infinite-item:first button').trigger('mouseover');
            //expect(cy.get('.infinite-item button')).to.have()
        }

    )
}
//Infinite Scroll_Verify the ‘Read more’ button is present for the 2nd article.
const scrollToSecondReadMore = () => {
    it('Infinite Scroll_Verify the ‘Read more’ button is present for the 2nd article.',
        () => {
            cy.get('.infinite-item:first button').scrollIntoView({ easing: 'linear', duration: 500 });
            expect(cy.get('.infinite-item:first').next()).to.exist;
            expect(cy.get('.infinite-item:first').next().find('button')).to.exist;
        }
    )
}
//Infinite Scroll_Continue scrolling down to the end of the page and then verify there are 5 ‘read more’ buttons at the end of the page.
const scrollToBottom = () => {
    it('Infinite Scroll_Continue scrolling down to the end of the page and then verify there are 5 ‘read more’ buttons at the end of the page.',
        () => {
            cy.get('.infinite-item:last button').next().scrollIntoView({ easing: 'linear', duration: 500 });
            cy.get('.infinite-item:last button').next().scrollIntoView({ easing: 'linear', duration: 500 });
            cy.get('.infinite-item:last button').next().scrollIntoView({ easing: 'linear', duration: 500 });
            cy.get('.infinite-item button').should('have.length', 5);
        }
    )
}
//Infinite Scroll_Click on the 1st ‘read more’ button. Verfy there are only 4 ‘read more’ buttons at the end of the page
const clickOnFirstReadMore = () => {
    it('Infinite Scroll_Verify the ‘Read more’ button is present for the 2nd article. Verfy there are only 4 ‘read more’ buttons at the end of the page',
        () => {
            cy.get('.infinite-item:first button').scrollIntoView({ easing: 'linear', duration: 500 , offset:{ top : 200-Cypress.config('viewportHeight') } });
            cy.get('.infinite-item:first button').click();
            cy.get('.infinite-item button').should('have.length', 4);
        }
    )
}

const test = (mobile) => {
    describe('Infinite scroll', () => {
        verifyArticeIsPresent(mobile);
        scrollToBottotomOfArticle(mobile);
        scrollToFirstReadMore(mobile);
        hoverFirstReadMore(mobile);
        scrollToSecondReadMore(mobile);
        scrollToBottom(mobile);
        clickOnFirstReadMore(mobile);
    });
}

exports.test = test;