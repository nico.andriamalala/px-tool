/// <reference types="Cypress" />

const utils = require('../../global_functions/utils');

//Going to the first Article page
const goToArticlePage = (environment,country) => {
    //Initialize fixture to retrieve url for Article
    cy.fixture('restage/urls').then(($urls)=>{
        const base =$urls[environment][country]['base']
        const relativePath = $urls[environment][country]['articleTemplate1']
        //cy.visit(base+relativePath)
        cy.visit("http://localhost:3000/");
    })
}

//Breadcrumb
const verifyBreadcrumbs = (mobile) => {

    //Selectors for Mobile/Desktop
    if(!mobile){ // if the viewport is desktop
        // 14.	Breadcrumb_Verify on desktop the breadcrumb has 4 sections
        // Example ONLY using reference: https://www.pampers.co.uk/pregnancy/baby-shower/article/baby-shower-games
        cy.get('.l-container .c-breadcrumb__list').then(($breadcrumbs) => {
            expect($breadcrumbs).to.contains.text('Home') //Verify Home text is present
            expect($breadcrumbs).to.contains.text('Pregnancy') //Verify Pregnancy text is present
            expect($breadcrumbs).to.contains.text('Baby Shower') //Verify Baby Shower text is present
            expect($breadcrumbs).to.contains.text('11 Types of Fun Baby Shower Games') //Verify Article text is present
            
        })
        
        // 15.	Breadcrumb_Verify on desktop the links on breadcrumb is present
        // Some actions here
        
    }else{ // agent is mobile
        // 16.	Breadcrumb_Verify on mobile the breadcrumb consists only category and subcategory
        cy.get('.post__category .category__wrapper').then(($breadcrumbs) => {
            expect($breadcrumbs).does.not.contains.text('Home') //Verify Home text is present
            expect($breadcrumbs).to.contains.text('Pregnancy') //Verify Pregnancy text is NOT present
            expect($breadcrumbs).to.contains.text('Baby Shower') //Verify Baby Shower text is NOT present
            expect($breadcrumbs).does.not.contains.text('11 Types of Fun Baby Shower Games') //Verify Article text is present
            
        })
        // 17.	Breadcrumb_Verify on mobile the breadcrumb has 2 links
        // Some actions here
    }

}

// CTA
const verifyCTAButton = (mobile) => {

    // 29.	CTA Button_Verify the button is present
    // Some actions here
    
    // 30.	CTA Button_Verify hyperlink is present
    // Some actions here

    //Selectors for Mobile/Desktop
    if(!mobile){ // if the viewport is desktop
        
    }else{ // agent is mobile

    }

}

//cypress custom functions
function routeReq(methodd,path,aliasName){
    // Create a route listener to try and catch
    cy.route({method: methodd ,url: path}).as(aliasName);
}

exports.goToArticlePage = goToArticlePage;
exports.verifyBreadcrumbs = verifyBreadcrumbs;
exports.verifyCTAButton = verifyCTAButton;