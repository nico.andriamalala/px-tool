/// <reference types="Cypress" />

//58.	Top calendar_Verify the component is present 
const VerifyPregnancyCalendarTopIsPresent = () => {
    it('Verify Pregnancy Calendar is present',
        () => {
            expect(cy.get('.preg-nav-top')).to.exist;
        }
    )
}

//59.	Top calendar_Verify only the next arrow is present when on the start of the series
//60.	Top calendar_Verify only the back arrow is present when on the end of the series
//61.	Top calendar_Verify both next and back arrow is present when in the middle of the series
const VerifyArrowTopIsPresent = () => {
    it('Verify arrow top is present',
        () => {
            expect(cy.get('.preg-nav-top').find('svg[xmlns="http://www.w3.org/2000/svg"]')).to.exist;
        }
    )
}

const test = () => {
    VerifyPregnancyCalendarTopIsPresent();
    VerifyArrowTopIsPresent();
}

exports.test = test;