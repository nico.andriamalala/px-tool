//62.	Top calendar_Verify the middle section corresponds to the current page of the article series
//63.	Bottom calendar_Verify the component is present
const VerifyPregnancyCalendarBottomIsPresent = () => {
    it('Verify Pregnancy Calendar bottom is present',
        () => {
            expect(cy.get('.preg-nav-bottom')).to.exist;
        }
    )
}
//64.	Bottom calendar_Verify only the next arrow is present when on the start of the series
//65.	Bottom calendar_Verify only the back arrow is present when on the end of the series
//66.	Bottom calendar_Verify both next and back arrow is present when in the middle of the series
const VerifyArrowBottomIsPresent = () => {
    it('Verify Pregnancy Calendar arrow bottom',
        () => {
            expect(cy.get('.preg-nav-bottom').find('svg[xmlns="http://www.w3.org/2000/svg"]')).to.exist;
        }
    )
}

//67.	Bottom calendar_Verify the icons of SVG format
const VerifyImgBottomIsPresent = () => {
    it('Verify Pregnancy Calendar bottom icon',
        () => {
            expect(cy.get('.preg-nav-bottom').find('img')).to.exist;
        }
    )
}


const test = () => {
    VerifyPregnancyCalendarBottomIsPresent();
    VerifyArrowBottomIsPresent();
    VerifyImgBottomIsPresent();
}

exports.test = test;