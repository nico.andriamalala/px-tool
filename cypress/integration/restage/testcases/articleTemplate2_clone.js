/// <reference types="Cypress" />

const utils = require('../../global_functions/utils');

// 12 - Progress bar_Verify the progress bar is present
const verifyProgressbarIsPressent = (mobile) => {
    cy.get('#stickyProgressBar .filler').then(($filler) => {
        expect($filler).to.exist;
    })
}

// 13 - Progress bar_Verify when the page is scrolled, the progress bar fills up
const verifyProgressbarChangeOnScroll = (mobile) => {
    cy.scrollTo(0, 1000 , { easing: 'linear' , duration: 1000});
    cy.get('#stickyProgressBar .filler').then(($filler) => {
        expect($filler).not.have.attr('value', '0');
    })
}

// 37 - App download_Verify the banner is present and shows the langing page
const verifyAppDownloadIsPresent = ( mobile ) => {
    cy.get('.appdownloadbody').then (($mainbody) =>{
        expect($mainbody).to.exist;
    });
    cy.get('.appdownloadbody').scrollIntoView({ easing: 'linear' , duration: 1000});
    expect(cy.get('.appdownload-lp')).to.exist;
    expect(cy.get('.appdownload-result-page')).to.exist;
}
const verifyAppDownloadShowsLandingPage = (mobile) =>{
    cy.get('.appdownload-lp').should('be.visible');
    cy.get('.appdownload-result-page').should('not.visible');
}
const verifyAppDownloadShowsResultPage = (mobile) =>{
    cy.get('.appdownload-lp').should('not.visible');
    cy.get('.appdownload-result-page').should('be.visible');
}
const verifyAppDownloadDiscoverNow = ( mobile ) => {
    if(mobile){
        cy.get('.appdownload-lp div.slick-active:first').click({ position: 'topLeft' });
        cy.wait(1000);
        cy.get('.appdownload-lp div.slick-active:first').click({ position: 'topLeft' });
        cy.wait(1000);
        cy.get('.appdownload-lp div.slick-active:first').click({ position: 'topLeft' });
        cy.wait(1000);
        cy.get('.appdownload-lp div.slick-active:first').click({ position: 'topLeft' });
        cy.wait(1000);
        cy.get('.appdownload-lp div.slick-active:last').click({ position: 'topLeft' });
        cy.wait(1000);
        cy.get('.appdownload-lp div.slick-active:last').click({ position: 'topLeft' });
        cy.wait(1000);
        cy.get('.appdownload-lp div.slick-active:last').click({ position: 'topLeft' });
        cy.wait(1000);
    }
    else{
        expect(cy.get('.appdownload-lp>.slick-slider button:first')).to.exist;
        expect(cy.get('.appdownload-lp>.slick-slider button:last')).to.exist;
        cy.get('.appdownload-lp .slick-slider button:first').click();
        cy.wait(1000);
        cy.get('.appdownload-lp .slick-slider button:first').click();
        cy.wait(1000);
        cy.get('.appdownload-lp .slick-slider button:first').click();
        cy.wait(1000);
        cy.get('.appdownload-lp .slick-slider button:first').click();
        cy.wait(1000);
        cy.get('.appdownload-lp .slick-slider button:last').click();
        cy.wait(1000);
        cy.get('.appdownload-lp .slick-slider button:last').click();
        cy.wait(1000);
        cy.get('.appdownload-lp .slick-slider button:last').click();
        cy.wait(1000);
    }
    cy.get('.appdownload-lp button.btn-disccover').click();
    cy.wait(2000);
}

const verifyAppDownloadAdjustLink = ( mobile ) => {    
    if(mobile){
        expect(cy.get('.appdownload-result-page .btn-footer a.cta-mobile')).to.exist;
        cy.get('.appdownload-result-page .btn-footer a.cta-mobile').should('have.attr', 'target', '_blank');
    }else{
        expect(cy.get('.appdownload-result-page .btn-footer a.cta-desktop')).to.exist;
        cy.get('.appdownload-result-page .btn-footer a.cta-desktop').should('have.attr', 'target', '_blank');
    }
}

// Verify recalculate link return to landing page
const verifyAppDownloadRecalculateLinkExist = ( mobile ) => {
    expect(cy.get('.appdownload-result-page>.recalculate-link a')).to.exist;
    cy.get('.appdownload-result-page .recalculate-link a').should('be.visible');
    cy.get('.appdownload-result-page .recalculate-link a').click();
    cy.wait(1000);
}

// 46 - HTML Table_Verify the HTML table is present
const verifyHtmlTableExist = ( mobile ) => {
    cy.get('.html-table').then (($mainbody) =>{
        expect($mainbody).to.exist;
    });
    cy.get('.html-table').scrollIntoView({ easing: 'linear' , duration: 1000});
}

// HTML Table_Verify on mobile the scroll functionality is present
const verifyHtmlTableScrollOnMobile = ( mobile ) => {
    if(mobile){
        cy.get('.html-table').should('have.css', 'overflow-x', 'auto');
    }
}

//Progress Bar
exports.verifyProgressbarIsPressent = verifyProgressbarIsPressent;
exports.verifyProgressbarChangeOnScroll = verifyProgressbarChangeOnScroll;
//App Download
exports.verifyAppDownloadIsPresent = verifyAppDownloadIsPresent;
exports.verifyAppDownloadShowsLandingPage = verifyAppDownloadShowsLandingPage;
exports.verifyAppDownloadShowsResultPage = verifyAppDownloadShowsResultPage;
exports.verifyAppDownloadDiscoverNow = verifyAppDownloadDiscoverNow;
exports.verifyAppDownloadAdjustLink = verifyAppDownloadAdjustLink;
exports.verifyAppDownloadRecalculateLinkExist = verifyAppDownloadRecalculateLinkExist;
//Html Table
exports.verifyHtmlTableExist = verifyHtmlTableExist;
exports.verifyHtmlTableScrollOnMobile = verifyHtmlTableScrollOnMobile;
