//All imports goes here
import { whitelabel } from './drivers/whitelabel_clone.spec'

//#################### EDIT BELOW INFO ONLY ##############################
//########################################################################
//Country defines the market in which cypress will execute
//Accceptable values on the array: whitelabel 
var country = ['whitelabel'] 

//Platform represents the device in which Cypress will execute the tests
//Accceptable values: desktop or mobile
var platform = 'desktop'

//Environment represents the domain as to where Cypress will be executed
//Current environments: production -> Production CD, staging -> Production CM
var environment = 'qa'  //dev

//Testsuite is the set of test cases defined for execution on the 'cypress.json' file
//To define your own testcases, update the 'custom' array
//Testsuites currently available: nnit, custom
var testsuite = 'nnit'  
//########################################################################
//########################################################################


//======================= DO NOT MODIFY DATA BELOW =====================//
//The below is an array loop which will through all the countries defined,
//and for each country, it will execute all 'test cases' for the 'testsuite'
country.forEach((market) => {
    if (market == 'whitelabel')
        whitelabel(environment, 'whitelabel', testsuite, platform)
    if (market == 'uk')
        UK(environment, 'uk', testsuite, platform) 
})