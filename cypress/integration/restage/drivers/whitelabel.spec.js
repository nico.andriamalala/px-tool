/// <reference types="Cypress" />

const articleTemplate1 = require('../testcases/articleTemplate1');
const articleTemplate2 = require('../testcases/articleTemplate2');
const articleTemplate2clone = require('../testcases/articleTemplate2_clone');

var describeTest = 'NNIT on Pampers '

export function whitelabel(environment,country,testsuite,viewport) {
    describe(describeTest + country, function () {

        //Get testcases for the testsuite defined on this run from Cypress.json
        var testcases = Cypress.env("testsuite")[country][testsuite]

        console.log('Testsuite: ' + '%c' + testsuite, "color:cyan") // returns the testsuite described on the runner.spec file
        console.log('Testcases: ' + '%c' + testcases, "color:cyan") // returns list of testcases defined for the testsuite
        console.log('%c' + environment, "color:cyan")
        console.log('%c' + country, "color:cyan")
        console.log('%c' + testsuite, "color:cyan")
        console.log('%c' + viewport, "color:cyan")
        
        beforeEach(()=>{
            //Set viewport to mobile if defined on the runner spec
            if(viewport == 'mobile')
                cy.viewport('iphone-x')
        })

        //Due to before() method creating a conflict on the Runner.spec, 
        //we have to create a context to visit the site and not use the before() method
        it("Go to Website", function () {
            cy.fixture("restage/urls").then((url)=>{
                //cy.visit(url[environment][country]['base'])
                cy.visit('localhost:3000');
            })
            //cy.clearCookies();
            //cy.clearLocalStorage();
        })

        // context("Article Template 1", function () {
        //     testcases.forEach((tcase) => {
        //         // cy.log(tcase)
        //         if (tcase == 'goToArticlePage') {
        //             if(viewport == 'mobile')
        //                 it('Go to Article Page', ()=>{articleTemplate2.goToArticlePage(environment,country)}) //true means mobile, country is 2nd variable
        //             else
        //                 it('Go to Article Page', ()=>{articleTemplate2.goToArticlePage(environment,country)}) //false means desktop, run desktop
        //         }

        //         if (tcase == 'verifyBreadcrumbs') {
        //             if(viewport == 'mobile')
        //                 it('verify Breadcrumbs', ()=>{articleTemplate2.verifyBreadcrumbs(true)}) //true means mobile, country is 2nd variable
        //             else
        //                 it('verify Breadcrumbs', ()=>{articleTemplate2.verifyBreadcrumbs(false)}) //false means desktop, run desktop
        //         }

        //         if (tcase == 'verifyCTAButton') {
        //             //CTA for example we do not have distinction between Desktop & Mobile
        //             it('verify CTA Button', ()=>{articleTemplate2.verifyCTAButton()}) 
        //         }
        //     })
        // })

        context("Article Template 2", function () {
            testcases.forEach((tcase) => {    
                if (tcase =='verifyAuthor') {
                    // it('Perform Author Test', ()=>{articleTemplate2.verifyAuthor()})
                }
                if (tcase =='verifyHeader') {
                    // it('Perform Author Test', ()=>{articleTemplate2.verifyAuthor()})
                }
                if (tcase =='verifyStickySummary') {
                    it('Perform StickySummary Test', ()=>{articleTemplate2.verifyStickySummary()})
                    it('Perform RelatedArticle Test', ()=>{articleTemplate2.verifyRelatedArticle()})
                    it('Perform SignUp Test', ()=>{articleTemplate2.verifySignup()})
                }
                if (tcase =='verifyBasicBanner') {
                    it('Perform BasicBanner Test', ()=>{articleTemplate2.verifyBasicBanner()})
                }
                if (tcase =='verifyBasicAccordion') {
                    it('Perform BasicBanner Test', ()=>{articleTemplate2.verifyBasicAccordion()})
                }
            })   
        })
    })
}