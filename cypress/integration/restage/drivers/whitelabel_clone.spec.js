/// <reference types="Cypress" />

const articleTemplate2 = require('../testcases/articleTemplate2_clone');
const infiniteScroll = require('../testcases/infiniteScroll');
const PregnancyCalendarTop = require('../testcases/PregnancyCalendarTop');
const PregnancyCalendarBottom = require('../testcases/PregnancyCalendarBottom');

var describeTest = 'NNIT on Pampers '

export function whitelabel(environment, country, testsuite, viewport) {
    describe(describeTest + country, function () {

        //Get testcases for the testsuite defined on this run from Cypress.json
        var testcases = Cypress.env("testsuite")[country][testsuite]

        console.log('Testsuite: ' + '%c' + testsuite, "color:cyan") // returns the testsuite described on the runner.spec file
        console.log('Testcases: ' + '%c' + testcases, "color:cyan") // returns list of testcases defined for the testsuite
        console.log('%c' + environment, "color:cyan")
        console.log('%c' + country, "color:cyan")
        console.log('%c' + testsuite, "color:cyan")
        console.log('%c' + viewport, "color:cyan")
        let mobile = viewport === 'mobile';
        beforeEach(() => {
            //Set viewport to mobile if defined on the runner spec
            if (viewport == 'mobile')
                cy.viewport('iphone-x')
        })

        //Due to before() method creating a conflict on the Runner.spec, 
        //we have to create a context to visit the site and not use the before() method
        it("Go to Website", function () {
            cy.fixture("restage/urls").then((url) => {
                //cy.visit(url[environment][country]['base'])
                cy.visit('localhost:3000');
            })
            //cy.clearCookies();
            //cy.clearLocalStorage();
        })

        // context("Article Template 1", function () {
        //     testcases.forEach((tcase) => {
        //         // cy.log(tcase)
        //         if (tcase == 'goToArticlePage') {
        //             if(viewport == 'mobile')
        //                 it('Go to Article Page', ()=>{articleTemplate2.goToArticlePage(environment,country)}) //true means mobile, country is 2nd variable
        //             else
        //                 it('Go to Article Page', ()=>{articleTemplate2.goToArticlePage(environment,country)}) //false means desktop, run desktop
        //         }

        //         if (tcase == 'verifyBreadcrumbs') {
        //             if(viewport == 'mobile')
        //                 it('verify Breadcrumbs', ()=>{articleTemplate2.verifyBreadcrumbs(true)}) //true means mobile, country is 2nd variable
        //             else
        //                 it('verify Breadcrumbs', ()=>{articleTemplate2.verifyBreadcrumbs(false)}) //false means desktop, run desktop
        //         }

        //         if (tcase == 'verifyCTAButton') {
        //             //CTA for example we do not have distinction between Desktop & Mobile
        //             it('verify CTA Button', ()=>{articleTemplate2.verifyCTAButton()}) 
        //         }
        //     })
        // })

        context("Article Template 2 - Clone", function () {
            testcases.forEach((tcase) => {
                if (tcase == 'verifyProgressBar') {
                    it('12 - Progress bar_Verify the progress bar is present'
                        , () => {
                            articleTemplate2.verifyProgressbarIsPressent(mobile);
                        });
                    it('13 - Progress bar_Verify when the page is scrolled, the progress bar fills up'
                        , () => {
                            articleTemplate2.verifyProgressbarChangeOnScroll(mobile);
                        });
                }
                if (tcase == 'verifyAdddownload') {
                    it('App download is present and show the Landing page',
                        () => {
                            articleTemplate2.verifyAppDownloadIsPresent(mobile);
                            articleTemplate2.verifyAppDownloadShowsLandingPage(mobile);
                        });
                    it('App download can select values and discover the result',
                        () => {
                            articleTemplate2.verifyAppDownloadDiscoverNow(mobile);
                            articleTemplate2.verifyAppDownloadShowsResultPage(mobile);
                        });
                    it('App download adjust link is present and opens in new tab',
                        () => {
                            articleTemplate2.verifyAppDownloadAdjustLink(mobile);
                        });
                    it('Verify recalculate link exist and return to landing page',
                        () => {
                            articleTemplate2.verifyAppDownloadRecalculateLinkExist(mobile);
                            articleTemplate2.verifyAppDownloadShowsLandingPage(mobile);
                        });
                }
                if (tcase == 'verifyHtmlTable') {
                    it('46 - HTML Table_Verify the HTML table is present',
                        () => { articleTemplate2.verifyHtmlTableExist(mobile) });
                    it('47 - HTML Table_Verify on mobile the scroll functionality is present',
                        () => { articleTemplate2.verifyHtmlTableScrollOnMobile(mobile) });
                }
                if (tcase == 'verifyInfinitScroll') {
                    infiniteScroll.test();
                }
                if (tcase == 'verifyPregnancyCalendar') {
                    PregnancyCalendarTop.test();
                }
                if (tcase == 'verifyPregnancyCalendarBottom') {
                    PregnancyCalendarBottom.test();
                }
            })
        })
    })
}