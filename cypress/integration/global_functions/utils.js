const emailGenerator = () => {
  var strValues="abcdefg12345"; 
  var strEmail = ""; 

  for (var i=0;i<10;i++) { 
    var strTmp = strValues.charAt(Math.round(strValues.length*Math.random())); 
    strEmail = strEmail + strTmp; 
  } 
  strEmail = strEmail + "@proximity.mu" 
  return strEmail; 
}

//Used for readFile() & writeFile()
//Concatenate the Folder path, file name of the file and it's file type
const pathToFile = (folderPath, fileName, fileExt) => {
  return folderPath + fileName + fileExt
}

exports.generateEmail = emailGenerator;
exports.pathToFile = pathToFile;