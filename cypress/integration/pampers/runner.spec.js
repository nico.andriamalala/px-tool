//All imports goes here
import { USEN } from './drivers/usenmodular.spec'
import { UK } from './drivers/ukmodular.spec'

//#################### EDIT BELOW INFO ONLY ##############################
//########################################################################
//Country defines the market in which cypress will execute
//Accceptable values on the array: us-en , uk
var country = ['us-en'] 

//Platform represents the device in which Cypress will execute the tests
//Accceptable values: desktop or mobile
var platform = 'desktop'

//Environment represents the domain as to where Cypress will be executed
//Current environments: production -> Production CD, staging -> Production CM
var environment = 'production'  

//Testsuite is the set of test cases defined for execution on the 'cypress.json' file
//To define your own testcases, update the 'custom' array
//Testsuites currently available: nnit, custom
var testsuite = 'nnit'  
//########################################################################
//########################################################################


//======================= DO NOT MODIFY DATA BELOW =====================//
//The below is an array loop which will through all the countries defined,
//and for each country, it will execute all 'test cases' for the 'testsuite'
country.forEach((market) => {
    if (market == 'us-en')
        USEN(environment, 'us-en', testsuite, platform)
    if (market == 'uk')
        UK(environment, 'uk', testsuite, platform) 
})