/// <reference types="Cypress" />

const utils = require('../../global_functions/utils');

const registration = (mobile,country) => {
    // Initialize listener on server
    cy.server()
    // Create a server to listen for popin response requestss
    routeReq('GET','/webservice/vortex/getpopin**','getpopin')  // method type, webservice path, alias name

    //Selectors for Mobile/Desktop
    //Registration button on the Header
    if(!mobile){ // if the viewport is desktop
        // Click on the Login button
        cy.xpath('.//*[contains(@class,"login-menu-option")]//a[contains(@class,"open-vortex-modal")]')
        .click({force:true})
        
    }else{ // agent is mobile
        //Click on the Hamburger menu
        cy.xpath('.//*[contains(@class,"js-menu-mobile-button")]')
            .click({force:true})        
        //Click on the Registration button
        cy.xpath('.//*[contains(@class,"js-login-mobile-oasis text-login profile__button event_button_click")]')
            .click()
    }

    // Using the route/server method we wait until the XHR for the popin appears, default timeout 5000ms
    cy.wait('@getpopin',5000)

    // Wait until the Registion pop-in has appeared and verify the Title text is present
    cy.waitUntil(() => cy.xpath('//*[@class="vortex janrain"]'))
    .find(".vortex__title")
    .should('contain.text',"Don't have an account yet?")
    .wait(2000)

    // Choose Child DOB
    cy.waitUntil(() => cy.get('.picker__button--today').click({force:true}))
    //.get('.picker__select--year').select('2018',{force:true})
    //.get('.picker__select--month > option:nth-child(12)').select('12',{force:true})
    //.get('.picker__table').select('31')

    // Fill in the first name 
    cy.waitUntil(() => 
        cy.get('#firstName').click({ force: true }))
        .type('CypressTestUser')

    // Generate random email
    var email = utils.generateEmail();

    // Update Test data file with new email address generated above //To be used on LOGIN and API
    //Read the testdata.json file found on the fixtures folder of Pampers US-EN 
    cy.readFile(utils.pathToFile('cypress/fixtures/pampers/test data/',country,'.json'))
    //then store the data found on this file onto the variable '$data'
        .then(($data) => {
            //Log the variable to see all the data present on this file
            //cy.log('File data: ' + $data)
            //Udate the 'email' variable found on this JSON file
            $data.credentials.email = email //Navigate to 'credentials' and then 'email', update the email address here
            
            //once the required field has been updated we need to write the WHOLE $data back to the 'testdata.json' file
            //Using the writeFile function we open the 'testdata.json' file and overwrite all the data by $data 
            //cy.writeFile(path , content)
            cy.writeFile(utils.pathToFile('cypress/fixtures/pampers/test data/',country,'.json')
                ,$data)

            //Log the variable to see if the new data has been overwritten without any old data missing
            //cy.log('File data: ' + $data)
    })
    
    // Fill in the email address 
    cy.waitUntil(() => 
        cy.get('#emailAddress').click({ force: true }))
        .type(email)
    //cy.get('#emailAddress').type('testemail01@proximity.mu')

    // Fill in the password 
    cy.waitUntil(() => 
        cy.get('#newPassword').click({ force: true }))
        .type('Testing123')

    //Initialize fixture to retrieve zip code
    cy.fixture('pampers/test data/us-en').then(($testdata)=>{
        cy.waitUntil(() => 
            cy.get('#addressPostalCode').click({ force: true }))
            .type($testdata.functions.registration.zipcode)
    })
    

    //Listen for PostRegistration
    routeReq('POST','/webservice/vortex/postregistration','postRegistration')

    //Click on the Registration button
    cy.xpath('.//*[contains(@class,"register-button")]').click()

    cy.wait('@postRegistration',3000)

    //#####################Verify Thank you Popin
    cy.waitUntil(() =>
        cy.get('.ajs-content')
            .should('contain.text', 'Your account has been created.')
    ).get('.ajs-close').click()


    //#####################Verify if user is logged on the Site

    //Selectors for Mobile/Desktop
    //Verify if the user is logged on the Site 
    if (!mobile) { // if the viewport is desktop
        cy.xpath('.//*[contains(@class,"option__authenticate")]//*[contains(@class,"event_profile_register")]')
            .should('include.text', 'Profile')
    } else {// device is mobile
        //Click on the Hamburger menu
        cy.xpath('.//*[contains(@class,"js-menu-mobile-button")]')
            .click({ force: true })
        //Get Profile element
        cy.get('.profile__button').then(($el) => {
            expect($el).to.include.text('Profile')
        })
    }
}

const logoutFromSite = (mobile) => {

    //Reload page before logging out
    //wait for 5 seconds
    //cy.reload()

    // Initialize listener on server
    cy.server()
    // Create a server to listen for popin response requests
    routeReq('POST','/webservice/logout','logout')  // method type, webservice path, alias name

    //Selectors for Mobile/Desktop
    //Verify if the user is logged on the Site 
    if(!mobile){ // if the viewport is desktop
        // Open the Profile dropdown
        cy.xpath('.//*[contains(@class,"js-login-menu-option")]').click()

        // Wait until cypress clicks on the Logout button
        cy.waitUntil(() => cy.xpath('.//*[contains(@class,"menu__option__item menu")]//*[contains(@class,"event_profile_logout")]')
            .click({force:true, multiple:true}))
    }else{ // device is mobile

        //Click on the Hamburger menu
        cy.xpath('.//*[contains(@class,"js-menu-mobile-button")]')
            .click({force: true})

        //Get Profile dropdown
        cy.get('.profile__button').click({ force: true })

        //Wait until cypress clicks on the Logout button
        cy.waitUntil(() => 
            cy.get('.section-profile-mobile .event_profile_logout')
                .click({ force: true, multiple: true }))
    }
    //Wait until the logout completes using the logout alias
    cy.wait('@logout',2000)

    if (!mobile) { // if the viewport is desktop
        // Wait until cypress locates the text 'log in' on the page
        cy.waitUntil(() => cy.xpath('.//*[contains(@class,"login-menu-option")]//a[contains(@class,"open-vortex-modal")]')
            .should('include.text', 'LOG IN'))
    } else { // device is mobile

        //Click on the Hamburger menu to verify if user is logged out
        cy.xpath('.//*[contains(@class,"js-menu-mobile-button")]')
            .click({ force: true })

        // Wait until cypress locates the text 'Sign Up' on the page
        cy.waitUntil(() => cy.get('.js-profile-container .event_button_click').then(($el) => {
            expect($el).to.include.text('Sign Up')
        }))
    }
}

const login = (country,mobile) => {

    // Initialize listener on server
    cy.server();
    // Create a server to listen for popin response requests
    routeReq('GET','/webservice/vortex/getpopin**','getpopin')

    //Selectors for Mobile/Desktop
    if(!mobile){ // if the viewport is desktop
        // Click on the Login button
        cy.xpath('.//*[contains(@class,"login-menu-option")]//a[contains(@class,"open-vortex-modal")]')
        .click({force:true})
    }else{ // device is mobile
        //Click on the Hamburger menu
        cy.xpath('.//*[contains(@class,"js-menu-mobile-button")]')
            .click({ force: true })     
    }

    //Selectors for Mobile/Desktop
    if(!mobile){ // if the viewport is desktop
        //Click on the 'Sign up' link found on end of the Registration form
        cy.xpath('.//*[@class="vortex__footer"]//a[contains(@class,"vortex-signin")]').click({force:true})
    
    } else { // device is mobile
        //Click on the Login button found on the Profile Navbar
        cy.xpath('//*[contains(@class,"profile__container")]//*[contains(@class,"event_profile_register")]')
            .click({force: true})
    }

    // Using the route/server method we wait until the XHR for the popin appears, default timeout 5000ms
    cy.wait('@getpopin',5000)
    //Initialize fixture to retrieve credentials
    cy.fixture('pampers/test data/'+ country).then(($testdata)=>{
        cy.waitUntil(() => 
            cy.get('#signInEmailAddress')
            .click({ force: true })  
        ).type($testdata.credentials.email, {force: true, delay: 50 })
        cy.waitUntil(() => 
            cy.get('#currentPassword')
            .click({ force: true })
        ).type($testdata.credentials.password, {force: true, delay: 50 })
    })

    routeReq('POST','/webservice/vortex/postlogin**','postlogin') // method type, webservice path, alias name
    cy.get('.js-login-button').click()
    cy.wait('@postlogin').then((req)=>{
        expect(req.status).to.eql(200)
    })
}

const performUnderage = (mobile) => {
    //reload page
    cy.reload()
    //Listen to server
    cy.server()
    //Selectors for Mobile/Desktop
    if(!mobile){ // if the viewport is desktop
        //Click on My Account on header section
        cy.xpath('.//*[contains(@class,"js-login-menu-option")]').click({force:true})
        //Click on Edit profile button on dropdown
        cy.xpath('.//*[contains(@class,"menu__option__item menu")]//*[contains(@class,"event_profile_update")]').click({force:true})
    }else{ //viewport is set to mobile
        //Click on the Hamburger menu
        cy.xpath('.//*[contains(@class,"js-menu-mobile-button")]').click({ force: true }) 
        //Click on Profile button  
        cy.get('.js-login-mobile-oasis').click({ force: true }) 
        //Click on Edit profile button on dropdown
        cy.get('.section-profile-mobile .event_profile_update').click({ force: true }) 
    }
    //Click on About me Section
    cy.xpath('.//*[contains(@class,"js-edit-profile-list-header")][2]').click({force:true})
    //Insert Lastname
    cy.xpath('.//*[@id="lastName"]').type('Hannah',{force:true})
    //Click on User DOB DT
    cy.xpath('.//*[@id="birthdate"]').click({force:true})
    //Click on Today
    cy.get('#birthdate_root .picker__button--today').click({force:true})
    // Create a server to listen for popin response requests
    routeReq('POST','/webservice/vortex/posteditprofile','postEditProfile')
    //Save Profile
    cy.xpath('.//*[contains(@class,"js-edit-profile-button")]').click({force:true})
    //Wait for postEditProfile to complete
    cy.wait('@postEditProfile').then((req)=>{
        expect(req.status).to.eql(200)
    })
    //Verify Underage Message
    cy.xpath('//div[@class="box box5"]').then(($msg) =>{
        expect($msg).to.have.class('box box5')
    })
}

const verifyAccountExistsJanrain = (country,) => {
    cy.fixture('pampers/test data/'+country).then((data)=>{
        cy.request({
            method: 'POST',
            url: 'https://procter-gamble.us.janraincapture.com/oauth/auth_native_traditional',
            body: {
                signInEmailAddress: data.credentials.email,
                currentPassword: data.credentials.password,
                form:'signInForm',
                redirect_uri:'https://localhost',
                response_type:'code_and_token',
                locale:'en-US',
                client_id:'94rzukfatrasdjettw6fbx8hm9gwp3tz',
                flow_version:'20181219143041919747',
                flow:'pampers_us'
            }
        })
        .then((res) => {
            if(res.body.stat == 'ok') { //Account present
                console.log(res.body)
                cy.log('Account exists at Janrain')
                expect(res.body.capture_user.email).to.eql(data.credentials.email)
            }
            if(res.body.stat == 'error'){ //Account not present at Janrain
                console.log(res.body)
                cy.log('Account successfully deleted from Janrain')
                expect(res.body.error).to.eql('invalid_credentials')
            }      
        })  
    })
}

const goToArticlePage = (environment,country) => {
    //Initialize fixture to retrieve url for Article
    cy.fixture('pampers/urls').then(($urls)=>{
        const base =$urls[environment][country]['base']
        const relativePath = $urls[environment][country]['articlev2']
        cy.visit(base+relativePath)
    })
}


//cypress custom functions
function routeReq(methodd,path,aliasName){
    // Create a route listener to try and catch
    cy.route({method: methodd ,url: path}).as(aliasName);
}

exports.performRegistration = registration;
exports.performLogout = logoutFromSite;
exports.performLogin = login;
exports.goToArticlePage = goToArticlePage;
exports.performUnderage = performUnderage;
exports.verifyAccountExistsJanrain = verifyAccountExistsJanrain;