/// <reference types="Cypress" />

const utils = require('../../global_functions/utils');

const registration = (mobile) => {
    // Initialize listener on server
    cy.server()
    // Create a server to listen for popin response requestss
    routeReq('GET','/webservice/vortex/**','getpopin')  // method type, webservice path, alias name

    //Selectors for Mobile/Desktop
    //Registration button on the Header
    if(!mobile){ // if the viewport is desktop
        // Click on the Login button
        cy.xpath('.//*[contains(@class,"login-menu-option")]//a[contains(@class,"open-vortex-modal")]')
        .click({force:true})
        
    }else{ // agent is mobile
        //Click on the Login button
        cy.get('.js-login-mobile-oasis')
            .click({force:true})
    }

    // Using the route/server method we wait until the XHR for the popin appears, default timeout 5000ms
    cy.wait('@getpopin',5000)

    // Wait until the Registion pop-in has appeared and verify the Title text is present
    cy.waitUntil(() => cy.xpath('//*[@class="vortex janrain"]'))
    .find(".vortex__title")
    .should('contain.text',"Don't have an account yet?")
    .wait(2000)

    // Fill in the email address 
    cy.waitUntil(() => 
        cy.get('#emailAddress').click({ force: true }))
        .type(utils.generateEmail())
    //cy.get('#emailAddress').type('testemail01@proximity.mu')

    // Fill in the password 
    cy.waitUntil(() => 
        cy.get('#newPassword').click({ force: true }))
        .type('Testing123')

    // Fill in the first name 
    cy.waitUntil(() => 
        cy.get('#firstName').click({ force: true }).type('CypressTestUser'))

    // Choose Child DOB - GDPR
    //Choose Yes
    cy.waitUntil(() => cy.get('#radio1')).check({force: true})

    //Choose DOB Today
    cy.get('.janrain__calender-block.js-mandatory-only-if-visible .js-date-picker-janrain-container .picker__button--today').click({force: true})

    //Initialize fixture to retrieve zip code // Not applicable on UK
    /*
    cy.fixture('pampers/test data/us-en').then(($testdata)=>{
        cy.waitUntil(() => 
            cy.get('#addressPostalCode').click({ force: true }))
            .type($testdata.functions.registration.zipcode)
    })
    */
    
    //Listen for PostRegistration
    routeReq('POST','/webservice/vortex/postregistration','postRegistration')

    //Click on the Registration button
    cy.xpath('.//*[contains(@class,"register-button")]').click({force: true})
    cy.wait('@postRegistration',3000)

    //#####################Verify Thank you Popin
    cy.waitUntil(() =>
        cy.get('.appDownload__title')
            .should('contain.text', 'Thank you for creating your Pampers account'))
            .get('.ajs-close')
            .click()

    //#####################Verify if user is logged on the Site
    //Selectors for Mobile/Desktop
    //Verify if the user is logged on the Site 
    if (!mobile) { // if the viewport is desktop
        cy.xpath('.//*[contains(@class,"option__authenticate")]//*[contains(@class,"event_profile_register")]').then(($el) => {
            console.log($el)
        })
    } else {// device is mobile
        //Check if icon is present
        cy.get('.section-profile-mobile .section-profile #ctl01_aProfile').should("include.text", 'Profile')
    }
}

const logoutFromSite = (mobile) => {
    // Initialize listener on server
    cy.server()
    // Create a server to listen for popin response requests
    routeReq('POST','/webservice/logout','logout')  // method type, webservice path, alias name

    //Selectors for Mobile/Desktop
    //Verify if the user is logged on the Site 
    if(!mobile){ // if the viewport is desktop
        // Open the Profile dropdown
        cy.xpath('.//*[contains(@class,"js-login-menu-option")]').click()

        // Wait until cypress clicks on the Logout button
        cy.waitUntil(() => cy.xpath('.//*[contains(@class,"menu__option__item menu")]//*[contains(@class,"event_profile_logout")]')
            .click({force:true,multiple: true }))
    }else{ // device is mobile

        //Get Profile dropdown
        cy.get('.js-login-mobile-oasis')
            .click({force: true})

        //Wait until cypress clicks on the Logout button
        cy.waitUntil(() => 
            cy.get('.section-profile-mobile .event_profile_logout')
                .click({ force: true, multiple: true }))
    }
    //Wait until the logout completes using the logout alias
    cy.wait('@logout',9000)

    if (!mobile) { // if the viewport is desktop
        // WVerify class is present after logout
        cy.waitUntil(() => cy.get('.js-open-vortex-modal').should('have.class','event_profile_register'))
    } else { // device is mobile

        // Verify class is present after logout
        cy.waitUntil(() => cy.get('.js-login-mobile-oasis').should('have.class','event_profile_register'))
    }
}

const login = (country,mobile) => {

    // Initialize listener on server
    cy.server();
    // Create a server to listen for popin response requests
    routeReq('GET','/webservice/vortex/getpopin**','getpopin')

    //Selectors for Mobile/Desktop
    if(!mobile){ // if the viewport is desktop
        // Click on the Login button
        cy.xpath('.//*[contains(@class,"login-menu-option")]//a[contains(@class,"open-vortex-modal")]')
        .click({force:true})
    }else{ // device is mobile
        //Click on the Login button on header
        cy.get('.js-login-mobile-oasis').click({ force: true })     
    }

    //Selectors for Mobile/Desktop
    if(!mobile){ // if the viewport is desktop
        //Click on the 'Sign up' link found on end of the Registration form
        cy.xpath('.//*[@class="vortex__footer"]//a[contains(@class,"vortex-signin")]').click({force:true})
    } else { // device is mobile
        //Click on the 'Sign up' link found on end of the Registration form
        cy.xpath('.//*[@class="vortex__footer"]//a[contains(@class,"vortex-signin")]')
            .click({force: true})
    }

    // Using the route/server method we wait until the XHR for the popin appears, default timeout 5000ms
    cy.wait('@getpopin',2000);
    
    //Initialize fixture to retrieve credentials
    cy.fixture('pampers/test data/'+ country).then(($testdata)=>{
        cy.waitUntil(() => 
            cy.get('#signInEmailAddress')
            .click({ force: true })  
        ).type($testdata.credentials.email, { delay: 50 })
        cy.waitUntil(() => 
            cy.get('#currentPassword')
            .click({ force: true })
        ).type($testdata.credentials.password, { delay: 50 })
    })

    routeReq('POST','/webservice/vortex/postlogin**','postlogin') // method type, webservice path, alias name
    cy.get('.js-login-button').click()
    cy.wait('@postlogin').then((req)=>{
        expect(req.status).to.eql(200)
    })
}

const goToArticlePage = (environment,country) => {
    //Initialize fixture to retrieve url for Article
    cy.fixture('pampers/urls').then(($urls)=>{
        const base =$urls[environment][country]['base']
        const relativePath = $urls[environment][country]['articlev2']
        cy.visit(base+relativePath)
    })
}

//cypress custom functions
function routeReq(methodd,path,aliasName){
    // Create a route listener to try and catch
    cy.route({method: methodd ,url: path}).as(aliasName);
}

exports.performRegistration = registration;
exports.performLogout = logoutFromSite;
exports.performLogin = login;
exports.goToArticlePage = goToArticlePage;