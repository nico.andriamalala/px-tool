/// <reference types="Cypress" />

const pampers = require('../testcases/uk_functions');

var describeTest = 'NNIT on Pampers '

export function UK(environment,country,testsuite,viewport) {
    describe(describeTest + country, function () {

        //Get testcases for the testsuite defined on this run from Cypress.json
        var testcases = Cypress.env("testsuite")[country][testsuite]

        console.log('Testsuite: '+'%c'+testsuite, "color:green; background-color:yellow") // returns the testsuite described on the runner.spec file
        console.log('Testcases: '+'%c'+testcases, "color:green; background-color:yellow") // returns list of testcases defined for the testsuite
        
        beforeEach(()=>{
            //Set viewport to mobile if defined on the runner spec
            if(viewport == 'mobile')
                cy.viewport('iphone-x')

            //cy.waitForResource('all.js', 'script')
            //cy.waitForResource('globaloasis.css','script')
        })

        //Due to before() method creating a conflict on the Runner.spec, 
        //we have to create a context to visit the site and not use the before() method
        it("Go to Website", function () {
            cy.fixture("pampers/urls").then((url)=>{
                cy.visit(url[environment][country]['base'])
            })
            //cy.clearCookies();
            //cy.clearLocalStorage();
        })

        context("Context 1: Registration, Login & Logout", function () {
            testcases.forEach((tcase) => {
                // cy.log(tcase)
                if (tcase == 'registration') {
                    if(viewport == 'mobile')
                        it('Perform Registration', ()=>{pampers.performRegistration(true)}) //true means mobile is set to true
                    else
                        it('Perform Registration', ()=>{pampers.performRegistration()}) // no arguments means, run desktop
                }
        
                if (tcase == 'thank_you_popin') {
                    it('Verify Thank you popin', function () {pampers.verifyThankYouPopin()})
                }

                if (tcase == 'logged_in') {
                    if(viewport == 'mobile')
                        it('Verify user is logged on the Site', function () {pampers.verifyUserIsLoggedin(true)}) //true means mobile is set to true
                    else
                        it('Verify user is logged on the Site', function () {pampers.verifyUserIsLoggedin()}) // no arguments means, run desktop
                }

                if (tcase == 'logout') {
                    if(viewport == 'mobile')
                        it('Logout from the Site', function () {pampers.performLogout(true)})//true means mobile is set to true
                    else
                        it('Logout from the Site', function () {pampers.performLogout()})// no arguments means, run desktop
                }

                if (tcase == 'login') {
                    if(viewport == 'mobile')
                        it('Login to site', function () {pampers.performLogin(country,true)}) // first argument is the Country, second is set to 'true' if it's a mobile device
                    else
                        it('Login to site', function () {pampers.performLogin(country)})// first argument is the Country, second if not present means desktop
                }
            })
        })

        context("Context 2: Article", function () {
            testcases.forEach((tcase) => {    
                if (tcase == 'article') {
                    if (viewport == 'mobile')
                        it('Go to Article V2', function () { pampers.goToArticlePage(environment,country,true) })//true means mobile is set to true
                    else
                        it('Go to Article V2', function () { pampers.goToArticlePage(environment,country) })// no arguments means, run desktop
                }
            })   
        })
    })
}