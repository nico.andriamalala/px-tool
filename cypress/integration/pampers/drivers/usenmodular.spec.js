/// <reference types="Cypress" />

const pampers = require('../testcases/us_functions');

var describeTest = 'NNIT on Pampers '

export function USEN(environment,country,testsuite,viewport) {
    describe(describeTest + country, function () {

        //Get testcases for the testsuite defined on this run from Cypress.json
        var testcases = Cypress.env("testsuite")[country][testsuite]

        console.log('Testsuite: ' + '%c' + testsuite, "color:cyan") // returns the testsuite described on the runner.spec file
        console.log('Testcases: ' + '%c' + testcases, "color:cyan") // returns list of testcases defined for the testsuite
        console.log('%c' + environment, "color:cyan")
        console.log('%c' + country, "color:cyan")
        console.log('%c' + testsuite, "color:cyan")
        console.log('%c' + viewport, "color:cyan")
        
        beforeEach(()=>{
            //Set viewport to mobile if defined on the runner spec
            if(viewport == 'mobile')
                cy.viewport('iphone-x')
        })

        //Due to before() method creating a conflict on the Runner.spec, 
        //we have to create a context to visit the site and not use the before() method
        it("Go to Website", function () {
            cy.fixture("pampers/urls").then((url)=>{
                cy.visit(url[environment][country]['base'])
            })
            //cy.clearCookies();
            //cy.clearLocalStorage();
        })

        context("Context 1: Demo", function () {
            testcases.forEach((tcase) => {
                // cy.log(tcase)
                if (tcase == 'registration') {
                    if(viewport == 'mobile')
                        it('Perform Registration', ()=>{pampers.performRegistration(true,country)}) //true means mobile, country is 2nd variable
                    else
                        it('Perform Registration', ()=>{pampers.performRegistration(false,country)}) //false means desktop, run desktop
                }

                if (tcase == 'logout') {
                    if(viewport == 'mobile')
                        it('Logout from the Site', function () {pampers.performLogout(true)})//true means mobile is set to true
                    else
                        it('Logout from the Site', function () {pampers.performLogout(false)})// false means desktop
                }

                if (tcase == 'login') {
                    if(viewport == 'mobile')
                        it('Login to site', function () {pampers.performLogin(country,true)}) // first argument is the Country, second is set to 'true' if it's a mobile device
                    else
                        it('Login to site', function () {pampers.performLogin(country,false)})// first argument is the Country, false means desktop
                }

                if (tcase == 'articles') {
                    if (viewport == 'mobile')
                        it('Go to Article V2', function () { pampers.goToArticlePage(true) })//true means mobile is set to true
                    else
                        it('Go to Article V2', function () { pampers.goToArticlePage(false) })// false means desktop
                }

                if (tcase == 'performUnderage') {
                    if (viewport == 'mobile')
                        it('Delete account using Underage', function () { pampers.performUnderage(true) })//true means mobile is set to true
                    else
                        it('Delete account using Underage', function () { pampers.performUnderage(false) })// false means desktop
                }

                if (tcase == 'verifyAccountExistsJanrain') {
                    if (viewport == 'mobile')
                        it('Verify account exists on Janrain', function () { pampers.verifyAccountExistsJanrain(country,true) })//true means mobile is set to true
                    else
                        it('Verify account exists on Janrain', function () { pampers.verifyAccountExistsJanrain(country,false) })// false means desktop
                }
            })
        })

        context("Context 2: Article", function () {
            testcases.forEach((tcase) => {    

            })   
        })
    })
}