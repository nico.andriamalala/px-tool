Installation Guide:

#######First time Cloning##############
If you do not have Node.js installed on your machine, download and install it at https://nodejs.org/en/download/
Next step, execute the file Deploy_Node.bat to install node_modules
#######################################

#######How to run automation script##############
Edit which country, environment, platform you want to execute via cypress\integration\pampers\runner.spec.js
Go to cypress\runner, execute the project batch file you are working on
#################################################